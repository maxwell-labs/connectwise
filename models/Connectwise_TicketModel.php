<?php
namespace Craft;

class Connectwise_TicketModel extends BaseModel
{
	protected function defineAttributes()
	{
		return [
			"id" => AttributeType::Number,
			"summary" => AttributeType::String,
			"recordType" => AttributeType::String,
			"board" => AttributeType::Mixed,
			"status" => AttributeType::Mixed,
			"project" => AttributeType::Mixed,
			"phase" => AttributeType::Mixed,
			"wbsCode" => AttributeType::String,
			"company" => AttributeType::Mixed,
			"site" => AttributeType::Mixed,
			"siteName" => AttributeType::String,
			"addressLine1" => AttributeType::String,
			"addressLine2" => AttributeType::String,
			"city" => AttributeType::String,
			"stateIdentifier" => AttributeType::String,
			"zip" => AttributeType::String,
			"country" => AttributeType::Mixed,
			"contact" => AttributeType::Mixed,
			"contactPhoneNumber" => AttributeType::String,
			"contactPhoneExtension" => AttributeType::String,
			"contactEmailAddress" => AttributeType::String,
			"type" => AttributeType::Mixed,
			"subType" => AttributeType::Mixed,
			"item" => AttributeType::Mixed,
			"team" => AttributeType::Mixed,
			"priority" => AttributeType::Mixed,
			"serviceLocation" => AttributeType::Mixed,
			"source" => AttributeType::Mixed,
			"requiredDate" => AttributeType::DateTime,
			"budgetHours" => AttributeType::Number,
			"opportunity" => AttributeType::Mixed,
			"agreement" => AttributeType::Mixed,
			"severity" => AttributeType::String,
			"impact" => AttributeType::String,
			"externalXRef" => AttributeType::String,
			"poNumber" => AttributeType::String,
			"knowledgeBaseCategoryId" => AttributeType::Number,
			"knowledgeBaseSubCategoryId" => AttributeType::Number,
			"allowAllClientsPortalView" => AttributeType::Bool,
			"customerUpdatedFlag" => AttributeType::Bool,
			"automaticEmailContactFlag" => AttributeType::Bool,
			"automaticEmailResourceFlag" => AttributeType::Bool,
			"automaticEmailCcFlag" => AttributeType::Bool,
			"automaticEmailCc" => AttributeType::String,
			"billingMethod" => AttributeType::String,
			"billingAmount" => AttributeType::Number,
			"initialDescription" => AttributeType::String,
			"initialInternalAnalysis" => AttributeType::String,
			"initialResolution" => AttributeType::String,
			"contactEmailLookup" => AttributeType::String,
			"processNotifications" => AttributeType::Bool,
			"skipCallback" => AttributeType::Bool,
			"closedDate" => AttributeType::DateTime,
			"closedBy" => AttributeType::String,
			"closedFlag" => AttributeType::Bool,
			"dateEntered" => AttributeType::DateTime,
			"enteredBy" => AttributeType::String,
			"actualHours" => AttributeType::Number,
			"approved" => AttributeType::Bool,
			"subBillingMethod" => AttributeType::String,
			"subBillingAmount" => AttributeType::Number,
			"subDateAccepted" => AttributeType::DateTime,
			"dateResolved" => AttributeType::DateTime,
			"dateResplan" => AttributeType::DateTime,
			"dateResponded" => AttributeType::DateTime,
			"resolveMinutes" => AttributeType::Number,
			"resPlanMinutes" => AttributeType::Number,
			"respondMinutes" => AttributeType::Number,
			"isInSla" => AttributeType::Bool,
			"knowledgeBaseLinkId" => AttributeType::Number,
			"knowledgeBaseLinkType" => AttributeType::String,
			"resources" => AttributeType::String,
			"parentTicketId" => AttributeType::Number,
			"hasChildTicket" => AttributeType::Bool,
			"_info" => AttributeType::Mixed,
		];
	}

	public function getCompany()
	{
		return craft()->connectwise_company->get($this->company->id);
	}

	public function getContact()
	{
		return craft()->connectwise_contact->get($this->contact->id);
	}

	public function getSite()
	{
		return craft()->connectwise_company->getSite($this->site->id, $this->company->id);
	}
}
