<?php
namespace Craft;

class Connectwise_InputController extends BaseElementsController
{
	public function actionGetModalBody()
	{
		$endpoint = craft()->request->getParam('endpoint');

		$this->renderTemplate('connectwise/modalbody', array(
			'endpoint' => $endpoint
		));
	}

	public function actionGetModalItems()
	{
		$endpoint = craft()->request->getParam('endpoint');
		if ($endpoint == 'contact')
		{
			$firstName = craft()->request->getParam('firstName');
			$lastName = craft()->request->getParam('lastName');

			$conditions = '';
			if($firstName != '') { $conditions .= "firstName contains '".$firstName."'"; }
			if($lastName != '')
			{
				if($conditions != '') { $conditions .= ' AND '; }
				$conditions .= "lastName contains '".$lastName."'";
			}

			$items = craft()->connectwise_contact->find($conditions, ['fields' => 'firstName, lastName, company, id']);
		}
		else
		{
			$search = craft()->request->getParam('search');

			switch ($endpoint) {
				case 'company':
					$items = craft()->connectwise_company->find("name contains '".$search."'");
					break;

				case 'configuration':
					$items = craft()->connectwise_configuration->find("name contains '".$search."'");
					break;

				default:
					$items = array();
					break;
			}
		}

		$this->renderTemplate('connectwise/modalitems', array(
			'endpoint' => $endpoint,
			'items' => $items,
		));
	}
}
