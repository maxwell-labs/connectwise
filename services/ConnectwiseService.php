<?php
namespace Craft;

class ConnectwiseService extends BaseApplicationComponent
{
	protected static function createClient()
	{
		return new \Guzzle\Http\Client(craft()->config->get('server', 'connectwise'), [
			'request.options' => [
				'headers' => ['Content-type' => 'application/vnd.connectwise.com+json; version=v2015_6'],
				'auth' => [
					craft()->config->get('companyId', 'connectwise').'+'.craft()->config->get('publicKey', 'connectwise'),
					craft()->config->get('privateKey', 'connectwise'),
					'Basic'
				]
			]
		]);
	}
}
