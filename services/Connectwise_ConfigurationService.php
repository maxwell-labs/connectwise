<?php
namespace Craft;

class Connectwise_ConfigurationService extends ConnectwiseService
{
	public function find($conditions = '', $options = [])
	{
		$client = static::createClient();
		$request = array_merge(
			[
				'conditions' => $conditions,
				'orderBy' => 'name',
			],
			$options
		);
		$response = $client->get('company/configurations', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_ConfigurationModel::populateModels($response->json());
		}
		else
		{
			return [];
		}
	}

	public function count($conditions = '')
	{
		$client = static::createClient();
		$request = ['conditions' => $conditions];
		$response = $client->get('company/configurations/count', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return $response->json()->count;
		}
		else
		{
			return null;
		}
	}

	public function get($id, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(!empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/configurations/'.$id, [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_ConfigurationModel::populateModel($response->json());
		}
		else
		{
			return null;
		}
	}
}
