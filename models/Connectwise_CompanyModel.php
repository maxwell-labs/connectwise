<?php
namespace Craft;

class Connectwise_CompanyModel extends BaseModel
{
	protected function defineAttributes()
	{
		return [
			"id" => AttributeType::Number,
			"identifier" => AttributeType::String,
			"name" => AttributeType::String,
			"status" => AttributeType::Mixed,
			"type" => AttributeType::Mixed,
			"addressLine1" => AttributeType::String,
			"addressLine2" => AttributeType::String,
			"city" => AttributeType::String,
			"state" => AttributeType::String,
			"zip" => AttributeType::String,
			"country" => AttributeType::Mixed,
			"phoneNumber" => AttributeType::String,
			"faxNumber" => AttributeType::String,
			"website" => AttributeType::String,
			"territoryId" => AttributeType::Number,
			"marketId" => AttributeType::Number,
			"accountNumber" => AttributeType::String,
			"defaultContactId" => AttributeType::Number,
			"dateAcquired" => AttributeType::DateTime,
			"sicCode" => AttributeType::Mixed,
			"parentCompany" => AttributeType::Mixed,
			"annualRevenue" => AttributeType::Number,
			"numberOfEmployees" => AttributeType::Number,
			"ownershipType" => AttributeType::Mixed,
			"timeZone" => AttributeType::Mixed,
			"leadSource" => AttributeType::String,
			"leadFlag" => AttributeType::Bool,
			"unsubscribeFlag" => AttributeType::Bool,
			"calendarId" => AttributeType::Number,
			"dateDeleted" => AttributeType::DateTime,
			"deletedBy" => AttributeType::String,
			"deletedFlag" => AttributeType::Bool,
			"_info" => AttributeType::Mixed,
			"customFields" => AttributeType::Mixed,
		];
	}

	public function getNotes($options = [])
	{
		return craft()->connectwise_company->getNotes($this->id, $options);
	}

	public function getSites($options = [])
	{
		return craft()->connectwise_company->getSites($this->id, $options);
	}

	public function getTeams($options = [])
	{
		return craft()->connectwise_company->getTeams($this->id, $options);
	}

	public function getContacts($conditions = '', $options = [])
	{
		return craft()->connectwise_contact->find('company/id = '.$this->id.$conditions, $options);
	}

	public function getConfigurations($conditions = '', $options = [])
	{
		return craft()->connectwise_configuration->find('company/id = '.$this->id.$conditions, $options);
	}

	public function getTickets($conditions = '', $options = [])
	{
		return craft()->connectwise_ticket->find('company/id = '.$this->id.$conditions, $options);
	}
}
