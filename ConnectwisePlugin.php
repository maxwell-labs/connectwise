<?php
namespace Craft;

class ConnectwisePlugin extends BasePlugin
{
	function getName()
	{
		return Craft::t('Connectwise');
	}

	function getVersion()
	{
		return '1.5';
	}

	function getDeveloper()
	{
		return 'Maxwell Labs';
	}

	function getDeveloperUrl()
	{
		return 'http://maxwell-labs.com';
	}
}
