<?php
namespace Craft;

class Connectwise_ConfigurationVariable
{
	public function find($conditions = '', $options = array())
	{
		return craft()->connectwise_configuration->find($conditions, $options);
	}

	public function byId($id)
	{
		return craft()->connectwise_configuration->get($id);
	}
}
