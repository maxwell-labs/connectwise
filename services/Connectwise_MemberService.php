<?php
namespace Craft;

class Connectwise_MemberService extends ConnectwiseService
{
	public function find($conditions = '', $options = [])
	{
		$client = static::createClient();
		$request = array_merge(
			[
				'conditions' => $conditions,
				'orderBy' => 'memberIdentifier',
			],
			$options
		);
		$response = $client->get('system/members', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_MemeberModel::populateModels($response->json());
		}
		else
		{
			return [];
		}
	}

	public function count($conditions = '')
	{
		$client = static::createClient();
		$request = ['conditions' => $conditions];
		$response = $client->get('system/members/count', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return $response->json()->count;
		}
		else
		{
			return null;
		}
	}

	public function get($id, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(!empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('system/members/'.$id, [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_MemeberModel::populateModel($response->json());
		}
		else
		{
			return null;
		}
	}
}
