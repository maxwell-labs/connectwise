<?php
namespace Craft;

/**
 * Class Connectwise_CompanyFieldType
 *
 * @author    Maxwell Labs <support@maxwell-labs.com>
 * @copyright Copyright (c) 2015, Maxwell Labs
 */
class Connectwise_CompanyFieldType extends BaseFieldType
{
	/**
	 * @inheritDoc IComponentType::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		return Craft::t('Company (Connectwise)');
	}

	/**
	 * @inheritDoc IFieldType::getInputHtml()
	 *
	 * @param string $name
	 * @param mixed  $value
	 *
	 * @return string
	 */
	public function getInputHtml($name, $value)
	{
		if(!$value instanceof Connectwise_CompanyModel)
		{
			$value = craft()->connectwise_company->get($value);
		}

		$id = craft()->templates->formatInputId($name);
		$formattedId = craft()->templates->namespaceInputId($id);
		craft()->templates->includeJsResource('connectwise/connectwise.js');
		craft()->templates->includeJs('new Connectwise.Input(' .
			'"'.$formattedId.'", "company"' .
		');');

		return craft()->templates->render('connectwise/company', array(
			'id' => $id,
			'name'  => $name,
			'value' => $value,
		));
	}

	/**
	 * @inheritDoc IFieldType::defineContentAttribute()
	 *
	 * @return mixed
	 */
	public function defineContentAttribute()
	{
		return array(AttributeType::Number);
	}

	/**
	 * Outputs an object so that syntax can be determined.
	 *
	 * @param string $value
	 * @return string|object
	 */
	public function prepValue($value)
	{
		if (craft()->request->isCpRequest())
		{
			return $value;
		}
		else
		{
			return craft()->connectwise_company->get($value);
		}
	}

	/**
	 * Just give me the number.
	 *
	 * @param string $value
	 * @return string|object
	 */
	public function prepValueFromPost($value)
	{
		return $value;
	}
}
