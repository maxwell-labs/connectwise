<?php
namespace Craft;

class Connectwise_ApiModel extends BaseModel implements \Countable
{
	private $_elementType;

	private $_endpoints = [
		// Company API
		'companies' => '/company/companies',
		'companyManagementSummarys' => '/company/companies/{companyId}/managementSummaryReports',
		'companyNotes' => '/company/companies/{companyId}/notes',
		'companySites' => '/company/companies/{companyId}/sites',
		'companyStatuses' => '/company/companies/statuses',
		'companyTeams' => '/company/companies/{companyId}/teams',
		'companyTypes' => '/company/companies/types',
		'configurations' => '/company/configurations',
		'configurationStatuses' => '/company/configurations/statuses',
		'configurationTypeQuestions' => '/company/configurations/types/{typeId}/questions',
		'configurationTypes' => '/company/configurations/types',
		'contactCommunications' => '/company/contacts/{contactId}/communications',
		'contactDepartments' => '/company/contacts/departments',
		'contactNotes' => '/company/contacts/{contactId}/notes',
		'contactRelationships' => '/company/contacts/relationships',
		'contacts' => '/company/contacts',
		'contactImage' => '/company/contacts/{contactId}/image', // No criteria other than contactId
		'contactPortalSecurity' => '/company/contacts/{contactId}/portalSecurity', // No criteria other than contactId
		'contactTracks' => '/company/contacts/{contactId}/tracks',
		'contactTypes' => '/company/contacts/types',
		// Expense API
		'expenseEntries' => '/expense/entries',
		'expenseTypes' => '/expense/types',
		// Finance API
		'additions' => '/finance/agreements/{agreementId}/additions',
		
		// Marketing API
		// Procurement API
		// Project API
		// Sales API
		// Schedule API
		// Service Desk API
		// System API
		// Time API
	]

	public function __construct($endpoint, $attributes)
	{
		$this->_endopint = $endpoint;

		parent::__construct($attributes);
	}
}
