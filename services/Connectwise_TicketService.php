<?php
namespace Craft;

class Connectwise_TicketService extends ConnectwiseService
{
	public function find($conditions = '', $options = [])
	{
		$client = static::createClient();
		$request = array_merge(
			[
				'conditions' => $conditions,
				'orderBy' => 'name',
			],
			$options
		);
		$response = $client->get('service/tickets', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_TicketModel::populateModels($response->json());
		}
		else
		{
			return [];
		}
	}

	public function count($conditions = '')
	{
		$client = static::createClient();
		$request = ['conditions' => $conditions];
		$response = $client->get('service/tickets/count', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return $response->json()->count;
		}
		else
		{
			return null;
		}
	}

	public function get($id, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(!empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('service/tickets/'.$id, [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_TicketModel::populateModel($response->json());
		}
		else
		{
			return null;
		}
	}

	public function getNotes($ticketId, $options = [])
	{
		$client = static::createClient();
		$response = $client->get('service/tickets/'.$ticketId.'/notes', [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return [];
		}
	}

	public function getNote($id, $ticketId)
	{
		$client = static::createClient();
		$request = [];
		if(empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('service/tickets/'.$ticketId.'/notes/'.$id, [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return null;
		}
	}
}
