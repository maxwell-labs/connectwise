<?php
namespace Craft;

class Connectwise_CompanyService extends ConnectwiseService
{
	public function find($conditions = '', $options = [])
	{
		$client = static::createClient();
		$request = array_merge(
			[
				'conditions' => $conditions,
				'orderBy' => 'name',
			],
			$options
		);
		$response = $client->get('company/companies', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_CompanyModel::populateModels($response->json());
		}
		else
		{
			return [];
		}
	}

	public function count($conditions = '')
	{
		$client = static::createClient();
		$request = ['conditions' => $conditions];
		$response = $client->get('company/companies/count', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return $response->json()->count;
		}
		else
		{
			return null;
		}
	}

	public function get($id, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(!empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/companies/'.$id, [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_CompanyModel::populateModel($response->json());
		}
		else
		{
			return null;
		}
	}

	public function getNotes($companyId, $options = [])
	{
		$client = static::createClient();
		$response = $client->get('company/companies/'.$companyId.'/notes', [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return [];
		}
	}

	public function getNote($id, $companyId, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/companies/'.$companyId.'/notes/'.$id, [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return null;
		}
	}

	public function getSites($companyId, $options = [])
	{
		$client = static::createClient();
		$response = $client->get('company/companies/'.$companyId.'/sites', [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return [];
		}
	}

	public function getSite($id, $companyId, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/companies/'.$companyId.'/sites/'.$id, [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return null;
		}
	}

	public function getTeams($companyId, $options = [])
	{
		$client = static::createClient();
		$response = $client->get('company/companies/'.$companyId.'/teams', [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return [];
		}
	}

	public function getTeam($id, $companyId, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/companies/'.$companyId.'/teams/'.$id, [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return null;
		}
	}
}
