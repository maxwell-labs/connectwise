<?php
namespace Craft;

class Connectwise_ContactVariable
{
	public function find($conditions = '', $options = array())
	{
		return craft()->connectwise_contact->find($conditions, $options);
	}

	public function count($conditions = '')
	{
		return craft()->connectwise_contact->count($conditions);
	}

	public function byId($id, $fields = '')
	{
		return craft()->connectwise_contact->get($id, $fields);
	}

	public function communicationItems($id)
	{
		return craft()->connectwise_contact->getCommunications($id);
	}

	public function notes($contactId)
	{
		return craft()->connectwise_contact->getNotes($companyId);
	}

	public function noteById($contactId, $noteId)
	{
		return craft()->connectwise_contact->getNote($companyId, $noteId);
	}
}
