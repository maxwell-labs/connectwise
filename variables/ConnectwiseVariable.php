<?php
namespace Craft;

class ConnectwiseVariable
{
	// API Criteria Model
	public function api($endpoint, $criteria)
	{
		return new Connectwise_ApiModel($endpoint, $criteria);
	}
}
