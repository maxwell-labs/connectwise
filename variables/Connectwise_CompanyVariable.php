<?php
namespace Craft;

class Connectwise_CompanyVariable
{
	// Companies
	public function getCompanies($conditions = '', $options = [])
	{
		return craft()->connectwise_company->getCompanies($conditions, $options);
	}

	public function getCompanyCount($conditions = '', $options = [])
	{
		return craft()->connectwise_company->getCompanyCount($conditions, $options);
	}

	public function getCompany($id, $fields = '')
	{
		return craft()->connectwise_company->getCompany($id, $fields);
	}

	// Company Management Summaries
	public function getManagementReports($companyId, $options = [])
	{
		return craft()->connectwise_companyManagementReports->getReports($companyId, $options = []);
	}

	public function getManagementReportCount($companyId, $options = [])
	{
		return craft()->connectwise_companyManagementReports->getReportCount($companyId, $options = []);
	}

	public function getManagementReport($id, $companyId, $fields = '')
	{
		return craft()->connectwise_companyManagementReports->getReport($id, $companyId, $fields);
	}

	// Company Notes
	public function getNotes($companyId, $options = [])
	{
		return craft()->connectwise_companyNotes->getNotes($companyId, $options);
	}

	public function getNoteCount($companyId, $options = [])
	{
		return craft()->connectwise_companyNotes->getNoteCount($companyId, $options);
	}

	public function getNote($id, $companyId, $fields = '')
	{
		return craft()->connectwise_companyNotes->getNote($id, $companyId, $fields);
	}

	// Company Sites
	public function getSites($companyId, $options = [])
	{
		return craft()->connectwise_companySites->getSites($companyId, $options);
	}

	public function getSiteCount($companyId, $options = [])
	{
		return craft()->connectwise_companySites->getSiteCount($companyId, $options);
	}

	public function getSite($id, $companyId, $fields = '')
	{
		return craft()->connectwise_companySites->getSite($id, $companyId, $fields);
	}

	// Company Statuses
	public function getStatuses($companyId, $options = [])
	{
		return craft()->connectwise_companyStatuses->getStatuses($companyId, $options);
	}

	public function getStatusCount($companyId, $options = [])
	{
		return craft()->connectwise_companyStatuses->getStatusCount($companyId, $options);
	}

	public function getStatus($id, $companyId, $fields = '')
	{
		return craft()->connectwise_companyStatuses->getStatus($id, $companyId, $fields);
	}

	// Company Teams
	public function getTeams($companyId, $options = [])
	{
		return craft()->connectwise_companyTeams->getTeams($companyId, $options);
	}

	public function getTeamCount($companyId, $options = [])
	{
		return craft()->connectwise_companyTeams->getTeamCount($companyId, $options);
	}

	public function getTeam($id, $companyId, $fields = '')
	{
		return craft()->connectwise_companyTeams->getTeam($id, $companyId, $fields);
	}

	// Company Types
	public function getTypes($companyId, $options = [])
	{
		return craft()->connectwise_companyTypes->getTypes($companyId, $options);
	}

	public function getTypeCount($companyId, $options = [])
	{
		return craft()->connectwise_companyTypes->getTypeCount($companyId, $options);
	}

	public function getType($id, $companyId, $fields = '')
	{
		return craft()->connectwise_companyTypes->getType($id, $companyId, $fields);
	}

	// Configurations
	public function getConfigurations($conditions = '', $options = [])
	{
		return craft()->connectwise_configurations->getConfigurations($conditions, $options);
	}

	public function getConfigurationCount($conditions = '', $options = [])
	{
		return craft()->connectwise_configurations->getConfigurationCount($conditions, $options);
	}

	public function getConfiguration($id, $fields = '')
	{
		return craft()->connectwise_configurations->getConfiguration($id, $fields);
	}

	// Configuration Statuses
	public function getConfigurationStatuses($conditions = '', $options = [])
	{
		return craft()->connectwise_configurationStatuses->getConfigurationStatuses($conditions, $options);
	}

	public function getConfigurationStatusCount($conditions = '', $options = [])
	{
		return craft()->connectwise_configurationStatuses->getConfigurationStatusCount($conditions, $options);
	}

	public function getConfigurationStatus($id, $fields = '')
	{
		return craft()->connectwise_configurationStatuses->getConfigurationStatus($id, $fields);
	}
}
