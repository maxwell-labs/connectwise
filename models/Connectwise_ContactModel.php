<?php
namespace Craft;

class Connectwise_ContactModel extends BaseModel
{
	protected function defineAttributes()
	{
		return [
			"id" => AttributeType::Number,
			"firstName" => AttributeType::String,
			"lastName" => AttributeType::String,
			"type" => AttributeType::Mixed,
			"company" => AttributeType::Mixed,
			"site" => AttributeType::Mixed,
			"addressLine1" => AttributeType::String,
			"addressLine2" => AttributeType::String,
			"city" => AttributeType::String,
			"state" => AttributeType::String,
			"zip" => AttributeType::String,
			"country" => AttributeType::String,
			"relationship" => AttributeType::Mixed,
			"department" => AttributeType::Mixed,
			"inactiveFlag" => AttributeType::Bool,
			"securityIdentifier" => AttributeType::String,
			"managerContactId" => AttributeType::Number,
			"assistantContactId" => AttributeType::Number,
			"title" => AttributeType::String,
			"school" => AttributeType::String,
			"nickName" => AttributeType::String,
			"marriedFlag" => AttributeType::Bool,
			"childrenFlag" => AttributeType::Bool,
			"significantOther" => AttributeType::String,
			"portalPassword" => AttributeType::String,
			"portalSecurityLevel" => AttributeType::Number,
			"disablePortalLoginFlag" => AttributeType::Bool,
			"unsubscribeFlag" => AttributeType::Bool,
			"gender" => AttributeType::String,
			"birthDay" => AttributeType::DateTime,
			"anniversary" => AttributeType::DateTime,
			"presence" => AttributeType::String,
			"_info" => AttributeType::Mixed,
			"customFields" => AttributeType::Mixed,
		];
	}

	public function name()
	{
		return $this->firstName . ' ' . $this->lastName;
	}

	public function getCompany($fields = '')
	{
		return craft()->connectwise_company->get($this->company->id, $fields);
	}

	public function getSite($fields = '')
	{
		return craft()->connectwise_company->getSite($this->site->id, $this->company->id, $fields);
	}

	public function getNotes($options = [])
	{
		return craft()->connectwise_contact->getNotes($this->id, $options);
	}

	public function getTickets($conditions = '', $options = [])
	{
		return craft()->connectwise_ticket->find('contact/id = '.$this->id.$conditions, $options);
	}
}
