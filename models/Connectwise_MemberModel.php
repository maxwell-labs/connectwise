<?php
namespace Craft;

class Connectwise_MemberModel extends BaseModel
{
	protected function defineAttributes()
	{
		return [
			"id" => AttributeType::Number,
			"identifier" => AttributeType::String,
			"emailAddress" => AttributeType::String,
			"name" => AttributeType::String,
			"calendarId" => AttributeType::Number,
			"inactiveFlag"  => AttributeType::Bool,
			"_info" => AttributeType::Mixed,
		];
	}
}
