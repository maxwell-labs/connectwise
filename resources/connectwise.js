(function($){

if (typeof Connectwise == 'undefined')
{
	Connectwise = {};
}

Connectwise.Input = Garnish.Base.extend({
	id: null,
	endpoint: null,
	$div: null,
	$input: null,
	$modal: null,

	init: function(id, endpoint)
	{
		this.id = id;
		this.endpoint = endpoint;
		this.$div = $('#'+this.id);
		this.$input = $('#'+this.id+' .connectwise-input');

		$('#'+this.id+' .btn.add:not(.disabled)').on('click', $.proxy(this.onChooseClick, this));
		$('#'+this.id+' .elements a.delete').on('click', $.proxy(this.onDeleteClick, this));
	},

	onChooseClick: function(e)
	{
		e.preventDefault();
		e.stopPropagation();
		var $trigger = $(e.target);

		if (!$trigger.data('_connectwiseModal'))
		{
			var modal = new Connectwise.Modal(this.endpoint,
			{
				resizable: true,
				autoShow: false,
				onSelect: $.proxy(function(object)
				{
					var element = '<div class="element removable" data-id="'+object.id+'" data-label="'+object.name+'">'+
						'<a class="delete icon" title="Remove"></a><div class="label"><span class="title">'+object.name+'</span></div></div>';
					this.$div.children('.elements').html(element);
					this.$input.val(object.id);
					$('#'+this.id+' .elements a.delete').on('click', $.proxy(this.onDeleteClick, this));
					$('#'+this.id+' .btn.add').addClass('disabled');
					$('#'+this.id+' .btn.add').off('click');
					return true;
				}, this)
			});

			$trigger.data('_connectwiseModal', modal);
		}
		$trigger.data('_connectwiseModal').show();
	},

	onDeleteClick: function(e)
	{
		e.preventDefault();
		e.stopPropagation();
		this.$div.children('.elements').html('');
		this.$input.val('');
		$('#'+this.id+' .btn.add').removeClass('disabled');
		$('#'+this.id+' .btn.add').on('click', $.proxy(this.onChooseClick, this));
		return true;
	}
});

Connectwise.Modal = Garnish.Modal.extend({
	endpoint: null,
	selectedObject: null,

	$body: null,
	$table: null,
	$buttons: null,
	$cancelBtn: null,
	$selectBtn: null,

	init: function(endpoint, settings)
	{
		this.endpoint = endpoint;
		this.setSettings(settings, Connectwise.Modal.defaults);

		// Build the modal
		var $container = $('<div class="modal elementselectormodal"></div>').appendTo(Garnish.$bod),
			$body = $('<div class="body"><div class="spinner big"></div></div>').appendTo($container),
			$footer = $('<div class="footer"/>').appendTo($container);

		this.base($container, this.settings);

		this.$buttons = $('<div class="buttons rightalign first"/>').appendTo($footer);
		this.$cancelBtn = $('<div class="btn">'+Craft.t('Cancel')+'</div>').appendTo(this.$buttons);
		this.$selectBtn = $('<div class="btn disabled submit">'+Craft.t('Select')+'</div>').appendTo(this.$buttons);

		this.$body = $body;

		this.addListener(this.$cancelBtn, 'activate', 'cancel');
		this.addListener(this.$selectBtn, 'activate', 'selectObject');
	},

	onFadeIn: function()
	{
		// Get the modal body HTML based on the settings
		var data = {
			endpoint: this.endpoint,
		};

		Craft.postActionRequest('connectwise/input/getModalBody', data, $.proxy(function(response, textStatus)
		{
			if (textStatus == 'success')
			{
				this.$body.html(response);
				this.$table = this.$body.find('.elements table.data');

				this.$body.on('change', '.toolbar input', $.proxy(function(e)
				{
					console.log('searching!')
					var data = {
						endpoint: this.endpoint,
					};
					if(this.endpoint == 'contact')
					{
						data.firstName = this.$body.find('.toolbar .firstname input').val();
						data.lastName = this.$body.find('.toolbar .lastname input').val();
					}
					else
					{
						data.search = this.$body.find('.toolbar .search input').val();
					}

					Craft.postActionRequest('connectwise/input/getModalItems', data, $.proxy(function(response, textStatus)
					{
						if (textStatus == 'success')
						{
							this.$table.children('tbody').html(response);
							this.$table.children('tbody').on('click', 'tr', $.proxy(function(e)
							{
								this.$table.find('tbody tr').removeClass('sel');
								$(e.currentTarget).addClass('sel');
								this.selectedObject = {
									id: $(e.currentTarget).data('id'),
									name: $(e.currentTarget).data('label'),
								};
								this.updateSelectBtnState();

							}, this));
						}

					}, this));

				}, this));
			}

		}, this));

		this.base();
	},

	updateSelectBtnState: function()
	{
		if (this.$selectBtn)
		{
			if (this.selectedObject)
			{
				this.enableSelectBtn();
			}
			else
			{
				this.disableSelectBtn();
			}
		}
	},

	enableSelectBtn: function()
	{
		this.$selectBtn.removeClass('disabled');
	},

	disableSelectBtn: function()
	{
		this.$selectBtn.addClass('disabled');
	},

	enableCancelBtn: function()
	{
		this.$cancelBtn.removeClass('disabled');
	},

	disableCancelBtn: function()
	{
		this.$cancelBtn.addClass('disabled');
	},

	cancel: function()
	{
		if (!this.$cancelBtn.hasClass('disabled'))
		{
			this.hide();
		}
	},

	selectObject: function()
	{
		this.onSelect(this.selectedObject);
		if (this.settings.hideOnSelect)
		{
			this.hide();
		}
	},

	onSelect: function(object)
	{
		this.settings.onSelect(object);
	},
},
{
	defaults: {
		resizable: true,
		criteria: null,
		multiSelect: false,
		hideOnSelect: true,
		onCancel: $.noop,
		onSelect: $.noop
	}
});

})(jQuery);
