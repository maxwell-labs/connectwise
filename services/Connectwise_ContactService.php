<?php
namespace Craft;

class Connectwise_ContactService extends ConnectwiseService
{
	public function find($conditions = '', $options = [])
	{
		$client = static::createClient();
		$request = array_merge(
			[
				'conditions' => $conditions,
				'orderBy' => 'lastName',
			],
			$options
		);
		$response = $client->get('company/contacts', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_ContactModel::populateModels($response->json());
		}
		else
		{
			return [];
		}
	}

	public function count($conditions = '')
	{
		$client = static::createClient();
		$request = ['conditions' => $conditions];
		$response = $client->get('company/contacts/count', [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return $response->json()->count;
		}
		else
		{
			return null;
		}
	}

	public function get($id, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(!empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/contacts/'.$id, [], ["query" => $request])->send();
		if ($response->isSuccessful())
		{
			return Connectwise_CompanyModel::populateModel($response->json());
		}
		else
		{
			return null;
		}
	}

	public function getCommunications($contactId, $options = [])
	{
		$client = static::createClient();
		$response = $client->get('company/contacts/'.$contactId.'/communications', [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return [];
		}
	}

	public function getCommunication($id, $contactId, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/contacts/'.$contactId.'/communications/'.$id, [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return null;
		}
	}

	public function getNotes($contactId, $options = [])
	{
		$client = static::createClient();
		$response = $client->get('company/contacts/'.$companyId.'/notes', [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return [];
		}
	}

	public function getNote($id, $contactId, $fields = '')
	{
		$client = static::createClient();
		$request = [];
		if(empty($fields)) { $request = ['fields' => $fields]; }
		$response = $client->get('company/contacts/'.$companyId.'/notes/'.$id, [], ["query" => $options])->send();
		if ($response->isSuccessful())
		{
			return $response->json();
		}
		else
		{
			return null;
		}
	}
}
