<?php
namespace Craft;

class Connectwise_ConfigurationModel extends BaseModel
{
	protected function defineAttributes()
	{
		return [
			"id" => AttributeType::Number,
			"name" => AttributeType::String,
			"type" => AttributeType::Mixed,
			"status" => AttributeType::Mixed,
			"company" => AttributeType::Mixed,
			"contact" => AttributeType::Mixed,
			"site" => AttributeType::Mixed,
			"locationId" => AttributeType::Number,
			"businessUnitId" => AttributeType::Number,
			"deviceIdentifier" => AttributeType::String,
			"serialNumber" => AttributeType::String,
			"modelNumber" => AttributeType::String,
			"tagNumber" => AttributeType::String,
			"purchaseDate" => AttributeType::DateTime,
			"installationDate" => AttributeType::DateTime,
			"installedBy" => AttributeType::String,
			"warrantyExpirationDate" => AttributeType::DateTime,
			"vendorNotes" => AttributeType::String,
			"notes" => AttributeType::String,
			"macAddress" => AttributeType::String,
			"lastLoginName" => AttributeType::String,
			"billFlag" => AttributeType::Bool,
			"backupSuccesses" => AttributeType::Number,
			"backupIncomplete" => AttributeType::Number,
			"backupFailed" => AttributeType::Number,
			"backupRestores" => AttributeType::Number,
			"lastBackupDate" => AttributeType::DateTime,
			"backupServerName" => AttributeType::String,
			"backupBillableSpaceGb" => AttributeType::Number,
			"backupProtectedDeviceList" => AttributeType::String,
			"backupYear" => AttributeType::Number,
			"backupMonth" => AttributeType::Number,
			"ipAddress" => AttributeType::String,
			"defaultGateway" => AttributeType::String,
			"osType" => AttributeType::String,
			"osInfo" => AttributeType::String,
			"cpuSpeed" => AttributeType::String,
			"ram" => AttributeType::String,
			"localHardDrives" => AttributeType::String,
			"parentConfigurationId" => AttributeType::Number,
			"vendor" => AttributeType::Mixed,
			"manufacturer" => AttributeType::Mixed,
			"questions" => AttributeType::Mixed,
			"activeFlag" => AttributeType::Bool,
			"_info" => AttributeType::Mixed,
		];
	}

	public function getCompany()
	{
		return craft()->connectwise_company->get($this->company->id);
	}

	public function getContact()
	{
		return craft()->connectwise_contact->get($this->contact->id);
	}

	public function getSite()
	{
		return craft()->connectwise_company->getSite($this->site->id, $this->company->id);
	}
}
