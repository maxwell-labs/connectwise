<?php
namespace Craft;

class Connectwise_TicketVariable
{
	public function find($conditions = '', $options = array())
	{
		return craft()->connectwise_ticket->find($conditions, $options);
	}

	public function count($conditions = '', $isOpen = true)
	{
		return craft()->connectwise_ticket->count($conditions, $isOpen);
	}

	public function byId($id)
	{
		return craft()->connectwise_ticket->get($id);
	}

	public function documents($ticketId)
	{
		return craft()->connectwise_ticket->getDocuments($ticketId);
	}

	public function configurations($ticketId)
	{
		return craft()->connectwise_ticket->getConfigurations($ticketId);
	}
}
